% chapter 2 exercise ii
% Create predicate married_couple(Wife, Husband), and define relationships
% mother_in_law, brother_in_law, and son_in_law.

parent(thomas, eric).
parent(thomas, matthew).
parent(caroline, eric).
parent(caroline, matthew).
parent(terri, nathan).
parent(terri, brandon).
parent(john, nathan).
parent(john, brandon).
parent(andy, emily).
parent(andy, jacob).
parent(amy, emily).
parent(amy, jacob).
parent(vicky, thomas).
parent(vicky, andy).
parent(vicky, terri).
parent(darby, thomas).
parent(darby, andy).
parent(darby, terri).

male(thomas).
male(eric).
male(matthew).
male(john).
male(nathan).
male(brandon).
male(andy).
male(jacob).
male(darby).

female(vicky).
female(caroline).
female(terri).
female(amy).
female(emily).

father(Dad, Child) :-
	parent(Dad, Child),
	male(Dad).

mother(Mother, Child) :-
	parent(Mother, Child),
	female(Mother).

procreated(Man, Woman) :-
	father(Man, Child),
	mother(Woman, Child).

procreated(Woman, Man) :-
	mother(Woman, Child),
	father(Man, Child).

half_sibling(Sib1, Sib2) :-
	father(Father1, Sib1),
	father(Father2, Sib2),
	not(Father1 = Father2),
	mother(Mother, Sib1),
	mother(Mother, Sib2),
	not(Sib1 = Sib2).

half_sibling(Sib1, Sib2) :-
	father(Father, Sib1),
	father(Father, Sib2),
	mother(Mother1, Sib1),
	mother(Mother2, Sib2),
	not(Mother1 = Mother2),
	not(Sib1 = Sib2).

full_sibling(Sib1,Sib2) :-
	father(Father, Sib1),
	father(Father, Sib2),
	mother(Mother, Sib1),
	mother(Mother, Sib2),
	not(Sib1 = Sib2).

sibling(Sib1, Sib2) :-
	half_sibling(Sib1, Sib2).

sibling(Sib1, Sib2) :-
	full_sibling(Sib1, Sib2).

brother(Brother, Sibling) :-
	sibling(Brother, Sibling),
	male(Brother).

sister(Sister, Sibling) :-
	sibling(Sister, Sibling),
	female(Sister).

uncle(Uncle,Person) :-
	brother(Uncle,Parent),
	parent(Parent,Person).

uncle(Uncle, Person) :-
	procreated(Uncle, Aunt),
	aunt(Aunt, Person).

aunt(Aunt, Person) :-
	sister(Aunt, Parent),
	parent(Parent, Person).

aunt(Aunt, Person) :-
	procreated(Aunt, Uncle),
	uncle(Uncle, Person).

niece(Niece, Person) :-
	female(Niece),
	parent(Parent, Niece),
	sibling(Parent, Person).

nephew(Nephew, Person) :-
	male(Nephew),
	parent(Parent, Nephew),
	sibling(Parent, Person).

cousin(Cousin1,Cousin2) :-
	parent(Parent1,Cousin1),
	parent(Parent2,Cousin2),
	sibling(Parent1,Parent2).

married_couple(Wife, Husband) :-
	procreated(Wife, Husband).

married_couple(Husband, Wife) :-
	procreated(Husband, Wife).

mother_in_law(Mil, Person) :-
	mother(Mil, Child),
	married_couple(Child, Person).

father_in_law(Fil, Person) :-
	father(Fil, Child),
	married_couple(Child, Person).

brother_in_law(Bil, Person) :-
	male(Bil),
	married_couple(Bil, Wife),
	sibling(Wife, Person).

brother_in_law(Bil, Person) :-
	brother(Bil, Sibling),
	married_couple(Sibling, Person).

sister_in_law(Sil, Person) :-
	female(Sil),
	married_couple(Sil, Husband),
	sibling(Husband, Person).

sister_in_law(Sil, Person) :-
	sister(Sil, Sibling),
	married_couple(Sibling, Person).

son_in_law(Sil, Person) :-
	male(Sil),
	(mother_in_law(Person, Sil) ;
	father_in_law(Person, Sil)).

daughter_in_law(Dil, Person) :-
	female(Dil),
	(mother_in_law(Person, Dil) ;
	father_in_law(Person, Dil)).
